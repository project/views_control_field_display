<?php

/**
 * @file
 * Provides implementations of the required functions to let Views know about our filter.
 */

/**
 * Implementation of hook_views_data().
 */
function views_control_field_display_views_data () {
  return array(
    'views_control_field_display' => array(
      'table' => array(
        'group' => t('Global'),
        'join'  => array('#global' => array()),
          /* I think this is the right way to make this a fake field; this
             is how Views does it in views.views.inc */
      ),
      'control_fields' => array(
        'title' => t('Control field display'),
        'help' => t('An always exposed filter that lets users dynamically change the available columns.'),
        'filter' => array(
          'handler' => 'views_control_field_display_handler_filter',
        ),
      ),
    ),
  );
}

/**
 * Implementation of hook_views_handlers().
 */
function views_control_field_display_views_handlers () {
  return array(
    'handlers' => array(
      'views_control_field_display_handler_filter' => array(
        'parent' => 'views_handler_filter',
      ),
    ),
  );
}

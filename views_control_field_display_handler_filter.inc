<?php

/**
 * @file
 * Views filter class that controls field display.
 *
 * @todo:
 * - fix the form display so it looks better
 * - add the remember option to the options_form
 */

class views_control_field_display_handler_filter extends views_handler_filter {
  const VIEWS_CONTROL_FIELD_DISPLAY_ALWAYS = "always";
  const VIEWS_CONTROL_FIELD_DISPLAY_ON = "on";
  const VIEWS_CONTROL_FIELD_DISPLAY_OFF = "off";
  
  
  /**
   * we are always exposed
   */
  function construct () {
    parent::construct();
    $this->options['exposed'] = TRUE;
  }
  
  /**
   * let Views know that we will have an option for fields.
   */
  function option_definition () {
    $options = parent::option_definition();
    
    $options['fields'] = array(
      'contains' => array(),
    );
    
    return $options;
  }
  
  /**
   * display a configuration form
   */
  function options_form (&$form, &$form_state) {
    // we don't use the standard method of exposing the form,
    // so we need to call this method ourself.
    if (empty($this->options['expose']['identifier'])) {
      $this->expose_options();
      $this->options['expose']['single'] = FALSE; // we always allow multiple
    }
    
    // provide a form item for each of the fields defined for the display
    $form['fields'] = array(
      '#tree' => TRUE,
    );
    
    $options = array(
      VIEWS_CONTROL_FIELD_DISPLAY_ALWAYS => t('Always display'),
      VIEWS_CONTROL_FIELD_DISPLAY_ON     => t('Controllable, default on'),
      VIEWS_CONTROL_FIELD_DISPLAY_OFF    => t('Controllable, default off'),
    );
    
    foreach ($this->view->display_handler->get_handlers('field') as $field => $handler) {
      $name = $handler->ui_name();
      if ($hander->options['exclude']) {
        $form['fields'][$field] = array(
          '#type'   => 'item',
          '#title'  => $name,
          '#prefix' => '<p>',
          '#value'  => t('This field has been excluded from the display. It cannot be controlled.'),
          '#suffix' => '</p>',
        );
      }
      else {
        // if we haven't configured this column, provide a reasonable default value
        if (empty($this->options['fields'][$field])) {
          $this->options['fields'][$field] = VIEWS_CONTROL_FIELD_DISPLAY_ALWAYS;
        }
        
        $form['fields'][$field] = array(
          '#type'          => 'radios',
          '#title'         => $name,
          '#options'       => $options,
          '#default_value' => $this->options['fields'][$field]
          
        );
      }
    }
    
    // add in the expose settings that we need to provide
    $form['expose']['identifier'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Filter identifier'),
      '#description'   => t('This will appear in the URL after the ? to identify this filter. Cannot be blank.'),
      '#default_value' => $this->options['expose']['identifier'],
      '#size'          => 40,
    );
    
    $form['expose']['label'] = array(
      '#type' => 'textfield',
      '#default_value' => $this->options['expose']['label'],
      '#title' => t('Label'),
      '#size' => 40,
    );
    
    // we need to pass through the other options added by the call to expose_options().
    foreach (array('use_operator', 'operator', 'remember', 'single', 'optional') as $value) {
      $form['expose'][$value] = array(
        '#type'  => 'value',
        '#value' => $this->options['expose'][$value],
      );
    }
  }
  
  /**
   * display the exposed form
   *
   * @todo -- possibly this should be moved by a hook so that it is located better
   */
  function exposed_form (&$form, &$form_state) {
    $identifier       = $this->options['expose']['identifier'];
    $identifier_check = $identifier . '_check_submit';
    
    $options       = array();
    $default_value = array();
    
    foreach ($this->options['fields'] as $field => $setting) {
      $handler = $this->view->display_handler->get_handler('field', $field);
      
      switch ($setting) {
        case VIEWS_CONTROL_FIELD_DISPLAY_ALWAYS:
          // do nothing
          break;
        
        case VIEWS_CONTROL_FIELD_DISPLAY_ON:
          // add the field to the list of possible fields
          // and to the list of currently displayed columns
          $options[$handler->options['id']] = $handler->options['label'];
          $default_value[$handler->options['id']] = $handler->options['id'];
          break;
        
        case VIEWS_CONTROL_FIELD_DISPLAY_OFF:
          // only add the field to the list of available fields
          $options[$handler->options['id']] = $handler->options['label'];
          break;
      }
    }
    
    // add the control form item
    $form[$identifier] = array(
      '#type'          => 'checkboxes',
      '#options'       => $options,
      '#default_value' => $default_value,
      // these aren't technically needed, but they are put in by other views classes, so I do too.
      '#multiple'      => TRUE,
      '#size'          => count($options),
    );
    
    // add a form item so we know when we are at an initial display
    // this is needed so that all columns can be turned off.
    $form[$identifier_check] = array(
      '#type' => 'hidden',
      '#value' => 1,
    );
    
    // because of the way that views handles this form, we need to copy
    // our default values into the input section of the form state. However,
    // because we want people to be able to completely turn off all columns, we
    // also need a check on $identifier_check.
    if (       !empty($form_state['exposed'])
            && !isset($form_state['input'][$identifier])
            && !isset($form_state['input'][$identifier_check])) {
      $form_state['input'][$identifier] = $default_value;
    }
    
  }
  
  /**
   * instead of modifying the query here, we use this hook to change the 'exposed'
   * setting on the various fields.
   */
  function query () {
    foreach ($this->view->exposed_data[$this->options['expose']['identifier']] as $field => $display) {
      $handler = $this->view->display_handler->get_handler('field', $field);
      if (!$display) {
        $handler->options['exclude'] = TRUE;
      }
    }
  }
}
